import React from 'react';
import ComponentCreator from '@docusaurus/ComponentCreator';

export default [
  {
    path: '/repoaula_2_complex/blog',
    component: ComponentCreator('/repoaula_2_complex/blog', 'ffa'),
    exact: true
  },
  {
    path: '/repoaula_2_complex/blog/archive',
    component: ComponentCreator('/repoaula_2_complex/blog/archive', 'fd7'),
    exact: true
  },
  {
    path: '/repoaula_2_complex/blog/first-blog-post',
    component: ComponentCreator('/repoaula_2_complex/blog/first-blog-post', '5ce'),
    exact: true
  },
  {
    path: '/repoaula_2_complex/blog/long-blog-post',
    component: ComponentCreator('/repoaula_2_complex/blog/long-blog-post', 'c7d'),
    exact: true
  },
  {
    path: '/repoaula_2_complex/blog/mdx-blog-post',
    component: ComponentCreator('/repoaula_2_complex/blog/mdx-blog-post', '28c'),
    exact: true
  },
  {
    path: '/repoaula_2_complex/blog/tags',
    component: ComponentCreator('/repoaula_2_complex/blog/tags', 'adf'),
    exact: true
  },
  {
    path: '/repoaula_2_complex/blog/tags/docusaurus',
    component: ComponentCreator('/repoaula_2_complex/blog/tags/docusaurus', '099'),
    exact: true
  },
  {
    path: '/repoaula_2_complex/blog/tags/facebook',
    component: ComponentCreator('/repoaula_2_complex/blog/tags/facebook', '6c8'),
    exact: true
  },
  {
    path: '/repoaula_2_complex/blog/tags/hello',
    component: ComponentCreator('/repoaula_2_complex/blog/tags/hello', '66d'),
    exact: true
  },
  {
    path: '/repoaula_2_complex/blog/tags/hola',
    component: ComponentCreator('/repoaula_2_complex/blog/tags/hola', 'f55'),
    exact: true
  },
  {
    path: '/repoaula_2_complex/blog/welcome',
    component: ComponentCreator('/repoaula_2_complex/blog/welcome', '42f'),
    exact: true
  },
  {
    path: '/repoaula_2_complex/markdown-page',
    component: ComponentCreator('/repoaula_2_complex/markdown-page', 'b06'),
    exact: true
  },
  {
    path: '/repoaula_2_complex/docs',
    component: ComponentCreator('/repoaula_2_complex/docs', '860'),
    routes: [
      {
        path: '/repoaula_2_complex/docs',
        component: ComponentCreator('/repoaula_2_complex/docs', '9df'),
        routes: [
          {
            path: '/repoaula_2_complex/docs',
            component: ComponentCreator('/repoaula_2_complex/docs', 'cee'),
            routes: [
              {
                path: '/repoaula_2_complex/docs/category/tutorial---basics',
                component: ComponentCreator('/repoaula_2_complex/docs/category/tutorial---basics', 'd93'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/repoaula_2_complex/docs/category/tutorial---extras',
                component: ComponentCreator('/repoaula_2_complex/docs/category/tutorial---extras', '527'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/repoaula_2_complex/docs/intro',
                component: ComponentCreator('/repoaula_2_complex/docs/intro', 'bc0'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/repoaula_2_complex/docs/tutorial-basics/congratulations',
                component: ComponentCreator('/repoaula_2_complex/docs/tutorial-basics/congratulations', '2ec'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/repoaula_2_complex/docs/tutorial-basics/create-a-blog-post',
                component: ComponentCreator('/repoaula_2_complex/docs/tutorial-basics/create-a-blog-post', 'c6f'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/repoaula_2_complex/docs/tutorial-basics/create-a-document',
                component: ComponentCreator('/repoaula_2_complex/docs/tutorial-basics/create-a-document', '905'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/repoaula_2_complex/docs/tutorial-basics/create-a-page',
                component: ComponentCreator('/repoaula_2_complex/docs/tutorial-basics/create-a-page', '654'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/repoaula_2_complex/docs/tutorial-basics/deploy-your-site',
                component: ComponentCreator('/repoaula_2_complex/docs/tutorial-basics/deploy-your-site', 'd7d'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/repoaula_2_complex/docs/tutorial-basics/markdown-features',
                component: ComponentCreator('/repoaula_2_complex/docs/tutorial-basics/markdown-features', '83d'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/repoaula_2_complex/docs/tutorial-extras/manage-docs-versions',
                component: ComponentCreator('/repoaula_2_complex/docs/tutorial-extras/manage-docs-versions', '8db'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/repoaula_2_complex/docs/tutorial-extras/translate-your-site',
                component: ComponentCreator('/repoaula_2_complex/docs/tutorial-extras/translate-your-site', '757'),
                exact: true,
                sidebar: "tutorialSidebar"
              }
            ]
          }
        ]
      }
    ]
  },
  {
    path: '/repoaula_2_complex/',
    component: ComponentCreator('/repoaula_2_complex/', 'b54'),
    exact: true
  },
  {
    path: '*',
    component: ComponentCreator('*'),
  },
];
